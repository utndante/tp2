/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guia.pkg2.sensores;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Dante
 */
public class Mecanico implements Observer{
    
    Auto observado;

    public Mecanico(Auto auto) {
    
        observado = auto;
        observado.addObserver(this);
    }

    @Override
    public void update(Observable auto, Object args) {
       
        Auto auto1 = (Auto) auto;
        Auto auto2 = (Auto) args;
        
        if(auto1.getAño() != auto2.getAño()){
            
            System.out.println("El año del auto cambio de " + auto2.getAño() + " a " + auto1.getAño());
        }
        
        if(!auto1.getColor().equals(auto2.getColor())){
            
            System.out.println("El color del auto cambio de " + auto2.getColor() + " a " + auto1.getColor());
        } 
        
        if(!auto2.getMarca().equals(auto1.getMarca())){
            
            System.out.println("La marca del auto cambio de " + auto2.getMarca() + " a " + auto1.getMarca());
        } 

        
        if(!auto1.getModelo().equals(auto2.getModelo())){
            
            System.out.println("El modelo del auto cambio de " + auto2.getModelo() + " a " + auto1.getModelo());
        }
        
        if(auto2.getNivelAceite() != auto1.getNivelAceite()){
            
            System.out.println("El nivel de aceite del auto cambio de " + auto2.getNivelAceite() + " a " + auto1.getNivelAceite());
        } 
        
        if(auto2.getNivelAgua()!= auto1.getNivelAgua()){
            
            System.out.println("El nivel de agua del auto cambio de " + auto2.getNivelAgua() + " a " + auto1.getNivelAgua());
        } 
        
        if(auto2.getPresionNeumaticos()!= auto1.getPresionNeumaticos()){
            
            System.out.println("El nivel de presion de los neumaticos del auto cambio de " + auto2.getPresionNeumaticos() + " a " + auto1.getPresionNeumaticos());
        } 
    }
}