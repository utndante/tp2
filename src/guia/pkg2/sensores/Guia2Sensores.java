/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guia.pkg2.sensores;

import static jdk.nashorn.internal.runtime.JSType.isString;

/**
 *
 * @author Dante
 */
public class Guia2Sensores {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Auto observado = new Auto("Fiesta","Amarillo","Ford",1983,80,60,70);
        Mecanico ojo = new Mecanico(observado);
        int cambioAceite = (int) (observado.getNivelAceite() * 0.5);
        int cambioAgua = (int) (observado.getNivelAgua()* 0.2);
        double cambioPresion = (double) (observado.getPresionNeumaticos()* 0.5);
        observado.setAño(1998);
        observado.setModelo("Mustang");
        observado.setColor("Azul");
        observado.setMarca("Chevrolet");
        observado.setNivelAceite(cambioAceite);
        observado.setNivelAgua(cambioAgua);
        observado.setPresionNeumaticos(cambioPresion);
        
        System.out.println("TRAE EL AUTO A ARREGLAR");
    }
}
