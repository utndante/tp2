/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guia.pkg2.sensores;

import java.util.Observable;
import java.lang.Cloneable;
/**
 *
 * @author Dante
 */
public class Auto extends Observable implements Cloneable {
    
    protected String modelo;
    protected String color;
    protected String marca;
    protected int año;
    protected int nivelAceite;
    protected double presionNeumaticos;
    protected int nivelAgua;
    
    public Auto(){ }
    
    public Auto(String modelo, String color, String marca, int año, int nivelAceite, double presionNeumaticos, int nivelAgua) {
        this.modelo = modelo;
        this.color = color;
        this.marca = marca;
        this.año = año;
        this.nivelAceite = nivelAceite;
        this.presionNeumaticos = presionNeumaticos;
        this.nivelAgua = nivelAgua;
    }

    public int getAño() {
        return año;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public int getNivelAceite() {
        return nivelAceite;
    }

    public int getNivelAgua() {
        return nivelAgua;
    }

    public double getPresionNeumaticos() {
        return presionNeumaticos;
    }

    public void setAño(int año) {
        try{
            Auto autoClonado  = (Auto)this.clone();
            this.año = año;
            this.setChanged();
            this.notifyObservers(autoClonado); 
        }
        catch(CloneNotSupportedException e){
            
            System.out.println("No cambio");
        }
    }

    public void setColor(String color) {
        try{
            Auto autoClonado  = (Auto)this.clone();
            this.color = color;
            this.setChanged();
            this.notifyObservers(autoClonado); 
        }
        catch(CloneNotSupportedException e){
            
            System.out.println("No cambio");
        }
    }

    public void setMarca(String marca) {
        try{
            Auto autoClonado  = (Auto)this.clone();
            this.marca = marca;
            this.setChanged();
            this.notifyObservers(autoClonado); 
        }
        catch(CloneNotSupportedException e){
            
            System.out.println("No cambio");
        }
    }

    public void setModelo(String modelo) {
        try{
            Auto autoClonado  = (Auto)this.clone();
            this.modelo = modelo;
            this.setChanged();
            this.notifyObservers(autoClonado); 
        }
        catch(CloneNotSupportedException e){
            
            System.out.println("No cambio");
        }
    }

    public void setNivelAceite(int nivelAceite) {
        try{
            Auto autoClonado  = (Auto)this.clone();
            this.nivelAceite = nivelAceite;
            this.setChanged();
            this.notifyObservers(autoClonado); 
        }
        catch(CloneNotSupportedException e){
            
            System.out.println("No cambio");
        }
 
    }

    public void setNivelAgua(int nivelAgua) {
        try{
            Auto autoClonado  = (Auto)this.clone();
            this.nivelAgua = nivelAgua;
            this.setChanged();
            this.notifyObservers(autoClonado); 
        }
        catch(CloneNotSupportedException e){
            
            System.out.println("No cambio");
        }
    }

    public void setPresionNeumaticos(double presionNeumaticos) {
        try{
            Auto autoClonado  = (Auto)this.clone();
            this.presionNeumaticos = presionNeumaticos;
            this.setChanged();
            this.notifyObservers(autoClonado); 
        }
        catch(CloneNotSupportedException e){
            
            System.out.println("No cambio");
        }
    }
    
    public Auto clone() throws CloneNotSupportedException{
    
        Auto auto = (Auto) super.clone();
        return auto;
    }
    
    
}
