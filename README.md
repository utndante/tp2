1)Ciclo del patrón Observer/Observable:
Lo primero que se ejecuta es la funcion setChange() dentro del observable. 
Este objecto notificará al observer mediante la funcion notifyAll(Object).
La funcion anterior notifica al observer que el valor de observable fue modificado.
Cuando esto sucede, como consecuencia se ejecuta el metodo update() que implementa el objeto observer.

2)Los parametros que se pasan en la funcion update son update(Object observable, Object args)
El objecto que se pasa como primer parametro, es el que contiene las modificaciones que se realizaron,
en cambio el args puede ser el objeto con los datos originales. Este ultimo parametro tambien puede ser
implementado como la variable que va a modificarse.